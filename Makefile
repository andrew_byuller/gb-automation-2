TARGET = prog

$(TARGET): main.o avb_lib.a
	gcc $^ -o $@

main.o: main.c
	gcc -c $< -o $@

avb_lib.a: avb_lib.o
	ar rcs $@ $^

avb_lib.o: avb_lib.c avb_lib.h
	gcc -c -o $@ $<

clean:
	rm -f *.o *.a $(TARGET)